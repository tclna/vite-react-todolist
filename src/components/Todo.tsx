import {X} from "react-feather";
import React from "react";
import TodoModel from "../models/TodoModel";

export interface TodoListItemProps extends TodoModel {
  onDelete: () => void,
}

const Todo = ({ text, icon, onDelete }: TodoListItemProps ) =>
  <li className={'list-group-item'}>
    <div className="row">
      <div className={'col fs-5'}>
          {icon} {text}
      </div>
      <div className={'col-auto'}>
        <button onClick={onDelete} className={'btn btn-link text-black-50'}>
          <X/>
        </button>
      </div>
    </div>
  </li>

export default Todo