import React, {useState, useRef} from "react";

const ICONS : {text: string, icon: string}[] = [
  {text: "bacon", icon: "🥓"},
  {text: "pancake", icon: "🥞"},
  {text: "croissant", icon: "🥐"},
  {text: "tangerine", icon: "🍊"},
  // {text: "waffle", icon: "🧇"}, // unavailable on mobile
  {text: "meat", icon: "🍖"},
  {text: "apple", icon: "🍎"},
  {text: "cookie", icon: "🍪"},
  {text: "banana", icon: "🍌"},
  {text: "doughnut", icon: "🍩"},
  {text: "watermelon", icon: "🍉"},
  {text: "muffin", icon: "🧁"},
]

const Form = ({addItem}: { addItem: (text: string, icon?: string) => void }) => {
  const inputEl = useRef<HTMLInputElement>(null);
  const [text, setText] = useState("")
  const [icon, setIcon] = useState("bacon")

  const onSubmit = (e: any) => {
    e.preventDefault();
    inputEl.current?.focus();
    if (!text.length) return;
    addItem(text, ICONS.find(i => i.text === icon)?.icon);
    setText("");
  }

  return (
    <div className="my-3 justify-content-end">
      <form onSubmit={onSubmit} className="input-group">
        <select value={icon} onChange={e => setIcon(e.target.value)} name="icon"
                className="btn btn-outline-secondary dropdown">
          {ICONS.map(({text, icon}) => (
            <option value={text}>{icon}</option>
          ))}
        </select>
        <input ref={inputEl} value={text} onChange={e => setText(e.target.value)} className="form-control fs-4"
               type="text"/>
        <button className="btn btn-lg btn-primary">Add</button>
      </form>
    </div>
  )
}

export default Form