import React from "react";

const Header = () =>
  <>
    <h1>📝 TodoList</h1>
    <p className="fs-5">Simple TodoList using React Hooks</p>
  </>

export default Header