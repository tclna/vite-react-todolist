import React, {useState} from "react"
import ListItem  from "./Todo"
import TodoModel from "../models/TodoModel"

interface ListProps {
  items: TodoModel[],
  onDelete: (id: string) => void
}

const List = ({items, onDelete}: ListProps) => (
  <ul className="list-group">
    {
      items.map(({id, ...props}) => (
        <ListItem key={id} id={id} onDelete={() => onDelete(id)} {...props} />
      ))
    }
  </ul>
)

export const useList = (todos: TodoModel[]): [JSX.Element, (todo: TodoModel) => void, (id: string) => void] => {
  const [list, setList] = useState(todos)

  const deleteItem = (id: string) => {
    setList(list.filter(li => li.id != id))
  };

  const addItem = (todo: TodoModel) => {
    setList(list.concat(todo))
  }

  return [
    (<List items={list} onDelete={deleteItem}/>),
    addItem,
    deleteItem
  ]
}

export default List
