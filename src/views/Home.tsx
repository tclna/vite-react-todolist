import React from "react"

import TodoModel, {createTodo} from "../models/TodoModel";

import {useList} from "../components/List";
import Form from "../components/Form";

import LYRICS from "../lyrics.json";

const initLyrics = (): TodoModel[] => {
  return LYRICS.map(({text, icon}) => createTodo({text, icon}))
}

const Home = () => {
  let [list, addTodo] = useList(initLyrics())

  let addItem = (text: string, icon?: string) => {
    addTodo(createTodo({text, icon}))
  }

  return (
    <>
      <Form addItem={addItem}/>
      {list}
    </>
  )
}

export default Home