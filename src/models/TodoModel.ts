import {nanoid} from "nanoid";

export interface TodoHead {
  id: string,
  creationDate: number
}

export interface TodoPayload {
  text: string,
  icon?: string,
}

export default interface TodoModel extends TodoHead, TodoPayload {}

export const createTodo = (todo: TodoPayload) : TodoModel => (
  {
    id: nanoid(),
    creationDate: Date.now(),
    ...todo
  }
)
