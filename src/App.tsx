import React from 'react'

import Home from './views/Home'
import Header from "./components/Header";

function App() {

  return (
    <div className="App">
      <div className="container my-md-5 my-sm-3">
        <Header/>
        <Home/>
      </div>
    </div>
  )
}

export default App
